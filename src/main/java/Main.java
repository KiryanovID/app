public class Main {
    public static void main(String[] args) {
        Action action = new Action();
        try {
            int id = Integer.parseInt(args[0]);
            String stringForResponce = action.getJsonString(id);
            String resultString = action.getResponce(stringForResponce);
            System.out.println(resultString);
        } catch (NumberFormatException e) {
            System.out.println("Введено не верный аргумент");
        } catch (Exception e) {
            System.out.println("User not found!");
        }
    }
}
